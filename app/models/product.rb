class Product < ActiveRecord::Base
  has_many :bonus_codes
  belongs_to :service
  belongs_to :validation_type

  validates :title, presence: true, length: { maximum: 250 }
                                          
  def self.code_and_product_exists?(params)
    product_exists?(params) &&
       BonusCode.where(code: params[:bonus_code]).exists?
  end
  
  def self.product_exists?(params)
    where(id: params[:product_id]).exists?
  end
end
