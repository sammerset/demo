class ValidationType < ActiveRecord::Base
  has_many :products
  
  validates :title, presence: true, length: { maximum: 250 }
  validates :uniq_code, presence: true, uniqueness: true, 
                                    length: { maximum: 250 }
  
  def via_service?
    uniq_code == 'via_service'
  end
end
