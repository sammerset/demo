class Service < ActiveRecord::Base

  has_many :products

  class << self
    def service_tv(product_id, code)
      false
      #TvPool.check_code(product_id, code)
    end

    def service_rtg(product_id, code)
      false
      #RtfPool.check_code(product_id, code)
    end

    def service_n(product_id, code)
      false
      #ThirdServicePool.check_code(product_id, code)
    end
  end

  validates :uniq_code, presence: true, uniqueness: true, 
                                           length: { maximum: 250 }

  def _valid?(params)
    self.class.send( "service_#{uniq_code}", 
                      params[:product_id], 
                      params[:bonus_code] )
  end
end
