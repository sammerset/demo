class BonusCode < ActiveRecord::Base
  validates :code, presence: true, length: { minimum: 7, maximum: 20 }

  def self.preload_code_purchased?(params)
    product = Product.find(params[:product_id])
    product.bonus_codes.where(code: params[:bonus_code]).exists?
  end
  
  def self.service_code_purchased?(params)
    product = Product.find(params[:product_id])
    if product.validation_type.via_service?
      return product.service._valid?(params)
    end
    false
  end
end
