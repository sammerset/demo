module Api::V1
  class BonusCodesController < BaseController
    def_param_group :product_bonus_code do
      param :product_id, String, "Product id", :required => true
      param :bonus_code, String, "Bonus code body", :required => true
    end

    api :GET, "/bonus_codes/validate", "Check bonus code"
    param_group :product_bonus_code

    def validate
      if Product.code_and_product_exists?(params)
        preload_code_validation
      elsif Product.product_exists?(params)
        service_code_validation
      else
        render text: "Not Found", status: 404
      end
    end
    
    private
    
    def preload_code_validation
      if BonusCode.preload_code_purchased?(params)
        render text: "Ok", status: 200
      else
        render text: "Forbidden", status: 403
      end
    end
    
    def service_code_validation
      if BonusCode.service_code_purchased?(params)
        render text: "Ok", status: 200
      else
        render text: "Not Found", status: 404
      end
    end
  end
end
