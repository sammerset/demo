Demo::Application.routes.draw do
  apipie

  scope :module => [:api, :v1], :path => '/' do
    resources :bonus_codes, only: [] do
      collection do
        get :validate
      end
    end
  end
end
