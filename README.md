API requirements

URL: /bonus_codes/validate
HTTP METHOD: GET
PARAMS: product_id, bonus_code

Sample request:
GET /bonus_codes/validate?product_id=12354345&bonus_code=68483737392

Sample responses:
HTTP 200 OK
or
HTTP 404 NOT FOUND if bonus code doesn’t belong to a product with provided product_id.
HTTP 403 FORBIDDEN if bonus code hasn’t been sold.
