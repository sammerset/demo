FactoryGirl.define do
  factory :product, class: Product do
    title "Halo 4"
    service_id nil
    validation_type_id 1
  end

  factory :product_with_service_code_validation, class: Product do
    title "Samsung Galaxy 4"
    validation_type_id 2
  end
end
