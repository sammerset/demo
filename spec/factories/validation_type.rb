FactoryGirl.define do
  factory :validation_type, class: ValidationType do
    title 'Preload'
    uniq_code 'preload'
  end

  factory :validation_type_via_service, class: ValidationType do
    title 'Via Service'
    uniq_code 'via_service'
  end
end
