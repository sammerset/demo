FactoryGirl.define do
  factory :bonus_code, class: BonusCode do
    code '123456789'
  end
end
