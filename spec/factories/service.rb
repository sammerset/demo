FactoryGirl.define do
  factory :service, class: Service do
    title 'TV'
    uniq_code 'tv'
  end
end
