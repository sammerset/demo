require 'rails_helper'

describe Api::V1::BaseController::BonusCodesController do
  before(:each) do
    @product = create(:product)
  end

  describe "GET bonus_codes/validate" do
    it "product and related bonus code" do
      bonus_code = create(:bonus_code)
      @product.bonus_codes << bonus_code
      get '/bonus_codes/validate', 
                    product_id: @product.id, bonus_code: bonus_code.code
      expect(response.status).to eq(200)
    end
    
    it "product and unrelated bonus code" do
      bonus_code = create(:bonus_code)
      get '/bonus_codes/validate', 
                    product_id: @product.id, bonus_code: bonus_code.code
      expect(response.status).to eq(403)
    end
    
    it "product is absent" do
      bonus_code = create(:bonus_code)
      get '/bonus_codes/validate', 
                    product_id: 1234567, bonus_code: bonus_code.code
      expect(response.status).to eq(404)
    end
    
    it "bonus code is unpurchased" do
      get '/bonus_codes/validate', 
                    product_id: 1234567, bonus_code: '43256775443'
      expect(response.status).to eq(404)
    end

    describe 'third-party service' do
      before(:each) do
        create(:validation_type_via_service)
        service = create(:service, uniq_code: 'tv')
        service.products << @product
      end

      it "validate service purchased code" do
        Service.stub(:service_tv).and_return(true)

        get '/bonus_codes/validate', 
                    product_id: @product.id, bonus_code: '111111111'
        expect(response.status).to eq(200)
      end

      it "not validate service purchased code" do
        Service.stub(:service_tv).and_return(false)

        get '/bonus_codes/validate', 
                    product_id: @product.id, bonus_code: '111111111'
        expect(response.status).to eq(404)
      end
    end
  end
end
