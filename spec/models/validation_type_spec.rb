require 'rails_helper'

describe ValidationType do

  before(:each) do
    create(:validation_type)
  end

  it { should validate_presence_of(:title).with_message('can\'t be blank')}
  it { should validate_length_of(:title).
        is_at_most(250).with_message('is too long (maximum is 250 characters)')}
  it { should validate_uniqueness_of(:uniq_code).
                        with_message('has already been taken')}      
  it { should validate_length_of(:uniq_code).
        is_at_most(250).with_message('is too long (maximum is 250 characters)')}  

  it "creation validation type" do
    expect(ValidationType.last.title).to eq("Preload")
  end

end
