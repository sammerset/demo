require 'rails_helper'

describe Product do

  before(:each) do
    @product = create(:product)
  end

  it { should validate_presence_of(:title).with_message('can\'t be blank')}
  it { should validate_length_of(:title).is_at_most(250).
                        with_message('is too long (maximum is 250 characters)')}

  it "creation product" do
    expect(Product.last.title).to eq("Halo 4")
  end

  it "validation existing code and product" do
    bonus_code = create( :bonus_code, code: 123456789 )
    @product.bonus_codes << bonus_code
    params = { product_id: @product.id, bonus_code: bonus_code.code }
    expect(Product.code_and_product_exists?(params)).to be true
  end

  it "validation not existing code" do
    params = { product_id: @product.id, bonus_code: 123456789 }
    expect(Product.code_and_product_exists?(params)).to be false
  end

  it "validation not existing product" do
    bonus_code = create( :bonus_code, code: 123456789 )
    params = { product_id: 12345, bonus_code: bonus_code.code }
    expect(Product.code_and_product_exists?(params)).to be false
  end
end
