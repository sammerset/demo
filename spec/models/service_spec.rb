require 'rails_helper'

describe Service do

  before(:each) do
    create(:service)
  end

  it { should validate_presence_of(:uniq_code).with_message('can\'t be blank')}
  it { should validate_length_of(:uniq_code).
        is_at_most(250).with_message('is too long (maximum is 250 characters)')}
  it { should validate_uniqueness_of(:uniq_code).
                        with_message('has already been taken')}

  it "creation service" do
    expect(Service.last.title).to eq("TV")
  end

  it "call own class method" do
    params = { product_id: '1', bonus_code: '2'}
    service = Service.last
    service._valid?(params)
    expect(Service).to respond_to(:service_tv).with(2).arguments
  end

end
