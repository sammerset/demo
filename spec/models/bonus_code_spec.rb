require 'rails_helper'

describe BonusCode do

  before(:each) do
    @bonus_code = create(:bonus_code)
  end

  it { should validate_presence_of(:code).with_message('can\'t be blank')}
  it { should validate_length_of(:code).is_at_least(7).
                        with_message('is too short (minimum is 7 characters)')}
  it { should validate_length_of(:code).is_at_most(20).
                        with_message('is too long (maximum is 20 characters)')}

  it "creation bonus code" do
    expect(BonusCode.last.code).to eq('123456789')
  end

  it "validation purchased code" do
    product = create( :product )
    product.bonus_codes << @bonus_code
    params = { product_id: product.id, bonus_code: @bonus_code.code }
    expect(BonusCode.preload_code_purchased?(params)).to be true
  end

  it "validation not purchased code" do
    product = create( :product )
    params = { product_id: product.id, bonus_code: '123456789' }
    expect(BonusCode.preload_code_purchased?(params)).to be false
  end
  
  describe 'service validation' do
    before(:each) do
      service = create(:service, uniq_code: 'tv')

      @product = create( :product )
      @product.validation_type = create(:validation_type_via_service)
      service.products << @product
    end

    it "validate service purchased code" do
      Service.stub(:service_tv).and_return(true)
      params = { product_id: @product.id, bonus_code: '1222332324' }

      expect(BonusCode.service_code_purchased?(params)).to be true
    end
  
    it "not validate service purchased code" do
      Service.stub(:service_tv).and_return(false)
      params = { product_id: @product.id, bonus_code: '1222332324' }

      expect(BonusCode.service_code_purchased?(params)).to be false
    end
  end  
end
