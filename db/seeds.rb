# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

services = Service.create([ { title: 'TV', uniq_code: 'tv' },
                            { title: 'RTG', uniq_code: 'rtg' } ])

preloaded_validation_type = ValidationType.
                   create( title: 'Preloaded', uniq_code: 'preloaded' )

service_validation_type = ValidationType.
                   create( title: 'Via Service', uniq_code: 'via_service' )                            

products = [ 
  { title: 'Halo 4', 
    validation_type_id: preloaded_validation_type.id,
    service_id: nil },
  { title: 'Samsung Galaxy 4', 
    validation_type_id: service_validation_type.id,
    service_id: services.first.id },
  { title: 'Skype $10', 
    validation_type_id: service_validation_type.id,
    service_id: services.second.id } ].map do |attrs|
  Product.create(attrs)
end

preloaded_validation_type.products.each do |product|
  product.bonus_codes << BonusCode.new(code: rand(10000000..20000000))
end

service_validation_type.products.
  first.bonus_codes << BonusCode.new(code: rand(10000000..20000000))
