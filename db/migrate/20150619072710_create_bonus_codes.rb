class CreateBonusCodes < ActiveRecord::Migration
  def change
    create_table :bonus_codes do |t|
      t.integer :product_id
      t.string :code

      t.timestamps
    end
  end
end
