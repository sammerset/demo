class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :title
      t.string :uniq_code

      t.timestamps
    end
  end
end
