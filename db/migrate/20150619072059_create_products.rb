class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.integer :service_id
      t.integer :validation_type_id

      t.timestamps
    end
  end
end
