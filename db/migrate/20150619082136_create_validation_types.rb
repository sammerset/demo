class CreateValidationTypes < ActiveRecord::Migration
  def change
    create_table :validation_types do |t|
      t.string :title
      t.string :uniq_code

      t.timestamps
    end
  end
end
